﻿using Core.Config;
using Core.Enums;
using OpenQA.Selenium;

namespace Core.Selenium.BrowserFatory
{
    public static class DriverFactory
    {
        public static readonly Dictionary<BrowserType, DriverBuilder> BrowserDictionary = new()
        {
            { BrowserType.Chrome, new ChromeDriverBuilder() }
        };

        public static IWebDriver GetDriver()
        {
            return BrowserDictionary[ConfigProvider.Browser.Type].GetDriver();
        }
    }
}
