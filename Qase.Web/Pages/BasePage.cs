﻿using Core.Config;
using Core.Selenium;

namespace Qase.Web.Pages
{
    public class BasePage : Base
    {
        protected readonly string workEmail = ConfigProvider.Settings.Email;
        protected readonly string password = ConfigProvider.Settings.Password;

        public BasePage()
        { }
    }
}
