﻿using Allure.Commons;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;


namespace Core.Reporting
{
    public class AllureReport
    {
        public readonly AllureLifecycle allure = AllureLifecycle.Instance;
        private readonly IWebDriver _driver;

        public AllureReport(IWebDriver driver)
        {
            _driver = driver;
        }

        public void GetErrorScreenshot()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                Screenshot screenshot = ((ITakesScreenshot)_driver).GetScreenshot();
                screenshot.SaveAsFile("allure-results/screenshot.png", ScreenshotImageFormat.Png);
                allure.AddAttachment("Screenshot", "image/png", "allure-results/screenshot.png");
            }
        }
    }
}
