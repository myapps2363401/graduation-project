﻿using DataProvider.DataDistributor;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;

namespace Qase.Web.Pages
{
    public class CasePage : BasePage
    {
        #region Basic

        [FindsBy(How = How.Id, Using = "title")]
        public readonly IWebElement titleInput;

        [FindsBy(How = How.XPath, Using = "//label[text()='Status']")]
        public readonly IWebElement statusDropDown;

        [FindsBy(How = How.XPath, Using = "//p[@data-placeholder='For example: We can authorize on page http://example.com/login']")]
        public readonly IWebElement descriptionInput;

        [FindsBy(How = How.XPath, Using = "//label[text()='Suite']")]
        public readonly IWebElement suiteDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Severity']")]
        public readonly IWebElement severityDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Priority']")]
        public readonly IWebElement priorityDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Type']")]
        public readonly IWebElement typeDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Layer']")]
        public readonly IWebElement layerDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Is flaky']")]
        public readonly IWebElement isFlakyDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Milestone']")]
        public readonly IWebElement milestoneDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Behavior']")]
        public readonly IWebElement behaviorDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Automation status']")]
        public readonly IWebElement automationStatusDropDown;

        #endregion

        #region Parameters

        [FindsBy(How = How.XPath, Using = "//span[text()=' Add step']")]
        public readonly IWebElement addStepButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='wfCJwA']")]
        public readonly IWebElement stepsContainer;

        #endregion

        [FindsBy(How = How.XPath, Using = "//button[@id='save-case']")]
        public readonly IWebElement saveButton;

        public CasePage()
        {
            PageFactory.InitElements(driver, this);
        }

        public CasePage SetTitle(string title)
        {
            titleInput.SendKeys(title);
            return this;
        }

        public CasePage SelectElementInDropDown<T>(IWebElement webElement, T element)
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(webElement));
            webElement.Click();
            (driver.FindElement(By.XPath($"//div[@class='RvSoL5 cfvQxI']//div[text()='{element}']"))).Click();
            return this;
        }

        public CasePage AddStep()
        {
            addStepButton.Click();
            return this;
        }

        public CasePage SetSteps()
        {
            var stepActions = stepsContainer.FindElements(By.XPath(".//p[@data-placeholder='Step Action']"));
            var stepData = stepsContainer.FindElements(By.XPath(".//p[@data-placeholder='Data']"));
            var stepExpectedResults = stepsContainer.FindElements(By.XPath(".//p[@data-placeholder='Expected result']"));

            stepActions.ToList().ForEach(f => action.Pause(TimeSpan.FromMilliseconds(300)).Click(f)
            .Pause(TimeSpan.FromMilliseconds(10)).SendKeys(StepDataGenerator.GetRandomStepEntity().StepAction).Perform());

            stepData.ToList().ForEach(f => action.Click(f).Pause(TimeSpan.FromMilliseconds(10))
            .SendKeys(StepDataGenerator.GetRandomStepEntity().Data).Perform());

            stepExpectedResults.ToList().ForEach(f => action.Click(f).Pause(TimeSpan.FromMilliseconds(10))
            .SendKeys(StepDataGenerator.GetRandomStepEntity().ExpectedResult).Perform());

            return this;
        }

        public ProjectPage SaveClick()
        {
            saveButton.Click();
            return new ProjectPage();
        }
    }
}
