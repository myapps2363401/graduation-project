﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;

namespace Qase.Web.Pages
{
    public class StartPage : BasePage
    {
        [FindsBy(How = How.Id, Using = "signup")]
        public readonly IWebElement startForFreeButton;

        [FindsBy(How = How.Id, Using = "signin")]
        public readonly IWebElement loginLink;

        public StartPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public SignUpPage GoToSignUp()
        {
            startForFreeButton.Click();
            return new SignUpPage();
        }

        public LoginPage GoToLogin()
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(loginLink));
            loginLink.Click();
            return new LoginPage();
        }
    }
}
