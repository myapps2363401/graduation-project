﻿using Qase.Web.Steps;

namespace Qase.Ui.Tests.AllTests
{
    [TestFixture]
    internal class ProjectTests : BaseUiTest
    {
        private ProjectSteps projectSteps;

        [SetUp]
        public void SetUp()
        {
            browser.Open(baseUrl);
            projectSteps = new();
        }

        [Category("Smoke")]
        [Test]
        public void AddCaseTest()
        {
            projectSteps.AddCase();
        }

        [TearDown]
        public void TearDown()
        {
            report.GetErrorScreenshot();
        }
    }
}
