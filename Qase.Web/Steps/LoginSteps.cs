﻿using OpenQA.Selenium;
using Qase.Web.Pages;
using SeleniumExtras.WaitHelpers;

namespace Qase.Web.Steps
{
    public class LoginSteps : BasePage
    {
        public readonly StartPage startPage = new();
        public readonly LoginPage loginPage = new();

        public LoginSteps() 
        { }

        public void Login()
        {
            startPage.GoToLogin();
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("email")));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("password")));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".g8S6ZA.URXZ1_.iVsStV")));
            loginPage.SetWorkEmail(workEmail)
                .SetPassword(password)
                .SetRememberMe();
        }

        public void GoToWorkspace()
        {
            loginPage.ClickLogin();
            wait.Until(ExpectedConditions.ElementExists(By.CssSelector(".DLrXJI")));
        }
    }
}
