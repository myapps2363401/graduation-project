﻿namespace Qase.Web.UI.RadioButtons
{
    public class MemberAccessDictionary
    {
        public static readonly Dictionary<string, string> dic = new()
        {
            { "AddAllMembersToThisProject", "Add all members to this project"},
            { "GroupAccess", "Group access"},
            { "DontAddMembers", "Don't add members"}
        };
    }
}
