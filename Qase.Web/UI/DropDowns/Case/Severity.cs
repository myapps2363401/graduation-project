﻿namespace Qase.Web.UI.DropDowns.Case
{
    public enum Severity
    {
        Blocker,
        Critical,
        Major,
        Normal,
        Minor,
        Trivial
    }
}
