﻿namespace Qase.Web.UI.DropDowns.Case
{
    public enum Behavior
    {
        Positive,
        Negative,
        Destructive
    }
}
