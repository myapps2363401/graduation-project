﻿using Qase.Web.Steps;

namespace Qase.Ui.Tests.AllTests
{
    [TestFixture]
    internal class CaseTests : BaseUiTest
    {
        private CaseSteps caseSteps;

        [SetUp]
        public void SetUp()
        {
            browser.Open(baseUrl);
            caseSteps = new();
        }


        [Category("Smoke")]
        [Test]
        public void CreateCaseTest()
        {
            caseSteps.CreateCase();
            Assert.IsTrue(caseSteps.casePage.saveButton.Displayed);
            caseSteps.SaveCase();
        }


        [TearDown]
        public void TearDown()
        {
            report.GetErrorScreenshot();
        }
    }
}
