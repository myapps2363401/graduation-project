using Core.Config;
using NUnit.Allure.Core;
using NUnit.Framework;
using Qase.Api.Tests.Client;

namespace Qase.Api.Tests.Tests
{
    [AllureNUnit]
    //[Parallelizable(ParallelScope.Fixtures)]
    public class BaseApiTest
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            QaseService.Client = new(new Uri(ConfigProvider.Browser.BaseApiUrl));
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            QaseService.Client.Dispose();
        }
    }
}
