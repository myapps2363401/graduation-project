﻿using Core.Selenium.BrowserFatory;
using OpenQA.Selenium;

namespace Core.Selenium
{
    public class Browser
    {
        private static readonly ThreadLocal<Browser> browserInstances = new();
        public IWebDriver? Driver { get; private set; }
        public static Browser? Instance => browserInstances.Value ?? (browserInstances.Value = new Browser());

        private Browser()
        {
            Driver = DriverFactory.GetDriver();
        }

        public void Open(string url)
        {
            Driver?.Navigate().GoToUrl(url);
        }

        public async void OpenAsync(string url)
        {
            await Task.Run(() => Open(url));
        }

        public void Close()
        {
            Driver?.Close();
            Driver?.Quit();
            Driver?.Dispose();
            browserInstances.Value = null;
            Driver = null;
        }
    }
}
