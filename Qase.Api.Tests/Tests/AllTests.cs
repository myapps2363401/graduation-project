﻿using DataProvider.DataDistributor;
using NUnit.Framework;
using Qase.Api.Tests.Client;
using Qase.Api.Tests.Entities;

namespace Qase.Api.Tests.Tests
{
    [TestFixture]
    internal class AllTests : BaseApiTest
    {
        private string _title = NewProjectDataGenerator.GetRandomNewProjectEntity().ProjectName;
        private string _code = NewProjectDataGenerator.GetRandomNewProjectEntity().ProjectCode;

        [Category("Smoke")]
        [Test,Order(1)]
        public void CreateProjectTest()
        {
            ProjectRequest project = new()
            {
                title = _title,
                code = _code
            };

            var result = QaseService.CreateProject(project);
            Assert.Multiple(() =>
            {
                Assert.That((int)QaseService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.status, Is.EqualTo(true));
            });

            _code = result.result.code;
        }

        [Category("Smoke")]
        [Test, Order(2)]
        public void CreateCaseTest()
        {
            CaseRequest testCase = new()
            {
                title = "TestCase",
                severity = 3,
                priority = 3,
                description = "Test description"
            };

            var result = QaseService.CreateCase(_code, testCase);
            Assert.Multiple(() =>
            {
                Assert.That((int)QaseService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.status, Is.EqualTo(true));
            });
        }

        [Category("CriticalPath")]
        [Test, Order(3)]
        public void DeleteProjectTest()
        {
            var result = QaseService.DeleteProject(_code);
            Assert.Multiple(() =>
            {
                Assert.That((int)QaseService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.status, Is.EqualTo(true));
            });
        }
    }
}
