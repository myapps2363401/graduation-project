﻿using DataProvider.DataDistributor;
using Qase.Web.Pages;
using Qase.Web.UI.RadioButtons;

namespace Qase.Web.Steps
{
    public class ProjectsSteps : BasePage
    {
        private readonly LoginSteps loginSteps = new();
        public readonly ProjectsPage projectsPage = new();

        public ProjectsSteps()
        {
            loginSteps.Login();
            loginSteps.GoToWorkspace();
        }

        public void AddNewPublicProject()
        {
            var project = NewProjectDataGenerator.GetRandomNewProjectEntity();
            projectsPage.ClickButton(projectsPage.createNewProjectButton)
                .SetField(projectsPage.projectNameInput, project.ProjectName)
                .SetField(projectsPage.projectCodeInput, project.ProjectCode)
                .SetField(projectsPage.descriptionInput, project.Description)
                .SetProjectAccessType(ProjectAccessTypeEnum.Public);
        }

        public void AddNewPrivateProject()
        {
            AddNewPublicProject();
            projectsPage.SetProjectAccessType(ProjectAccessTypeEnum.Private)
                .SetMemberAccess(MemberAccessDictionary.dic["AddAllMembersToThisProject"]);
        }

        public void CreateProject()
        {
            projectsPage.CreateProject();
        }
    }
}
