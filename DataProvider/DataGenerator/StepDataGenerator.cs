﻿using DataProvider.Models;

namespace DataProvider.DataDistributor
{
    public class StepDataGenerator
    {
        public static StepEntity GetRandomStepEntity()
        {
            return new StepEntity()
            {
                StepAction = Faker.Internet.Url(),
                Data = Faker.Company.BS(),
                ExpectedResult = Faker.Internet.Email()
            };
        }
    }
}
