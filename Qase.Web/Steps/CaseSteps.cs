﻿using Qase.Web.Pages;
using Qase.Web.UI.DropDowns.Case;

namespace Qase.Web.Steps
{
    public class CaseSteps : BasePage
    {
        private readonly ProjectSteps projectSteps = new();
        public readonly CasePage casePage = new();

        public CaseSteps()
        {
            projectSteps.AddCase();
        }

        public void CreateCase()
        {
            casePage.SetTitle(Faker.Name.First())
                .SelectElementInDropDown(casePage.statusDropDown, Status.Draft)
                .SelectElementInDropDown(casePage.severityDown, Severity.Critical)
                .SelectElementInDropDown(casePage.priorityDown, Priority.Medium)
                .SelectElementInDropDown(casePage.typeDropDown, UI.DropDowns.Case.Type.Smoke)
                .SelectElementInDropDown(casePage.layerDropDown, Layer.E2E)
                .SelectElementInDropDown(casePage.behaviorDropDown, Behavior.Positive);

            casePage.AddStep()
                .AddStep()
                .AddStep()
                .SetSteps();
        }

        public void SaveCase()
        {
            casePage.SaveClick();
        }
    }
}
