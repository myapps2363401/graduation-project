﻿using Core.Config.ModelsConfig;
using Microsoft.Extensions.Configuration;

namespace Core.Config
{
    public static class ConfigProvider
    {
        private static IConfigurationRoot GetConfiguration
        {
            get
            {
                //var environmentName = Environment.GetEnvironmentVariable("TEST_ENV") ?? "DEV";
                return new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   //.AddJsonFile($"config.{environmentName}.json", true, false)
                   .AddJsonFile($"config.UAT.json", true, false)
                   .Build();
            }
        }

        private static IConfigurationRoot GetSettings
        {
            get
            {
                return new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile($"Settings.json", true, false)
                   .Build();
            }
        }

        public static BrowserConfig? Browser => GetConfiguration.GetSection("Browser").Get<BrowserConfig>();
        public static Settings? Settings => GetSettings.GetSection("Settings").Get<Settings>();

        public static string? GetSetting(string key)
        {
            return GetConfiguration[key];
        }

        public static T? GetGenericVal<T>(string key)
        {
            return GetConfiguration.GetValue<T>(key);
        }
    }
}
