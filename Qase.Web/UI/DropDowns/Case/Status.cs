﻿namespace Qase.Web.UI.DropDowns.Case
{
    public enum Status
    {
        Actual,
        Draft,
        Deprecated
    }
}
