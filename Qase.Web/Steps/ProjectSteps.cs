﻿using Qase.Web.Pages;

namespace Qase.Web.Steps
{
    public class ProjectSteps : BasePage
    {
        private readonly ProjectsSteps newProjectSteps = new();
        public readonly ProjectPage projectPage = new();

        public ProjectSteps()
        {
            newProjectSteps.AddNewPublicProject();
            newProjectSteps.CreateProject();
        }

        public void AddCase()
        {
            projectPage.CreateCase();
        }
    }
}
