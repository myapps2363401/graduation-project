﻿using DataProvider.Models;

namespace DataProvider.DataDistributor
{
    public class NewProjectDataGenerator
    {
        public static NewProjectEntity GetRandomNewProjectEntity()
        {
            return new NewProjectEntity()
            {
                ProjectName = Faker.Company.Name(),
                ProjectCode = Faker.Address.CityPrefix(),
                Description = Faker.Company.CatchPhrase()
            };
        }
    }
}
