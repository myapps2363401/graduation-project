﻿
namespace DataProvider.Models
{
    public class NewProjectEntity
    {
        public string? ProjectName { get; set; }
        public string? ProjectCode { get; set; }
        public string? Description { get; set; }
    }
}
