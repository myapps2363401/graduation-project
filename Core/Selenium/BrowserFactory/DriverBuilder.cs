﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Core.Selenium.BrowserFatory
{
    public abstract class DriverBuilder
    {
        public abstract IWebDriver GetDriver();

        protected void AddBaseOptions(ChromeOptions chromeOptions)
        {
            //chromeOptions.PageLoadStrategy = PageLoadStrategy.Normal;
            chromeOptions.PageLoadStrategy = PageLoadStrategy.Eager;
            chromeOptions.AddUserProfilePreference("credentials_enable_service", false);
            chromeOptions.AddUserProfilePreference("profile.password_manager_enabled", false);
            chromeOptions.AddArgument("--disable-cache");
            chromeOptions.AddExcludedArgument("enable-automation");
            chromeOptions.AddAdditionalChromeOption("useAutomationExtension", false);
            //chromeOptions.AddArguments("start-maximized");
            chromeOptions.AddArguments("--window-size=1920,1080");
        }
    }
}
