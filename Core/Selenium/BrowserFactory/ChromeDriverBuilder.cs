﻿using Core.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Core.Selenium.BrowserFatory
{
    public class ChromeDriverBuilder : DriverBuilder
    {
        protected ChromeOptions chromeOptions = new();

        public ChromeDriverBuilder()
        {
            AddBaseOptions(chromeOptions);
        }

        public override IWebDriver GetDriver()
        {
            if (ConfigProvider.Browser.HeadLess)
            {
                chromeOptions.AddArgument("--headless");
            }

            var chromeDriver = new ChromeDriver(chromeOptions);
            return chromeDriver;
            //var remoteUrl = Environment.GetEnvironmentVariable("SELENIUM_REMOTE_URL") ?? "http://localhost:4444/wd/hub";
            //return new RemoteWebDriver(new Uri(remoteUrl), chromeOptions);
        }
    }
}
