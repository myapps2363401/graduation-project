using Core.Config;
using Qase.Api.Tests.Entities;
using RestSharp;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Qase.Api.Tests.Client
{
    public static class QaseService
    {
        public static RestClient Client { get; set; }
        public static RestResponse Response { get; set; }
        public static RestRequest Request { get; set; }

        public static RestRequest SetRequest(string url, Method method)
        {
            return new RestRequest(url, method);
        }

        public static RestResponse SendRequest(string url, Method method)
        {
            Response = Client.ExecuteAsync(SetRequest(url, method)).Result;
            return Response;
        }

        public static RestResponse SendRequest(RestRequest request)
        {
            Response = Client.ExecuteAsync(request).Result;
            return Response;
        }

        public static JsonSerializerOptions GetJsonSerializerOptions()
        {
            return new JsonSerializerOptions()
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault
            };
        }

        public static ProjectResponse CreateProject(ProjectRequest project)
        {
            string json = JsonSerializer.Serialize<ProjectRequest>(project, GetJsonSerializerOptions());
            var request = SetRequest("/v1/project", Method.Post);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(json);
            request.AddHeader("Token", ConfigProvider.Settings.Token);
            var response = SendRequest(request).Content;

            return JsonSerializer.Deserialize<ProjectResponse>(response);
        }

        public static ProjectResponse DeleteProject(string code)
        {
            var request = SetRequest($"/v1/project/{code}", Method.Delete);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Token", ConfigProvider.Settings.Token);
            var response = SendRequest(request).Content;

            return JsonSerializer.Deserialize<ProjectResponse>(response);
        }

        public static CaseResponse CreateCase(string code, CaseRequest testCase)
        {
            string json = JsonSerializer.Serialize<CaseRequest>(testCase, GetJsonSerializerOptions());
            var request = SetRequest($"/v1/case/{code}", Method.Post);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(json);
            request.AddHeader("Token", ConfigProvider.Settings.Token);
            var response = SendRequest(request).Content;

            return JsonSerializer.Deserialize<CaseResponse>(response);
        }
    }
}
