﻿using Core.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Serilog;

namespace Core.Selenium
{
    public class Base
    {
        protected Browser browser;
        protected IWebDriver driver;
        protected static ILogger logger;
        protected WebDriverWait wait;
        protected IJavaScriptExecutor js;
        protected SelectElement dropDown;
        protected readonly string baseUrl = ConfigProvider.Browser.BaseUrl;
        protected Actions action;

        public Base()
        {
            browser = Browser.Instance;
            driver = Browser.Instance.Driver;
            logger = Core.Logger.Instance;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            js = (IJavaScriptExecutor)driver;
            action = new(driver);
        }
    }
}
