﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Qase.Web.Pages
{
    public class LoginPage : BasePage
    {
        [FindsBy(How = How.Name, Using = "email")]
        public readonly IWebElement workEmailInput;

        [FindsBy(How = How.Name, Using = "password")]
        public readonly IWebElement passwordInput;

        [FindsBy(How = How.Name, Using = "remember")]
        public readonly IWebElement rememberMeCheckbox;

        [FindsBy(How = How.CssSelector, Using = ".g8S6ZA.URXZ1_.iVsStV")]
        public readonly IWebElement signInButton;

        public LoginPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public LoginPage SetWorkEmail(string email)
        {
            action.Pause(TimeSpan.FromSeconds(1))
                .Click(workEmailInput)
                .Pause(TimeSpan.FromMilliseconds(100))
                .SendKeys(email)
                .Perform();
            return this;
        }

        public LoginPage SetPassword(string password)
        {
            action.Click(passwordInput)
                .Pause(TimeSpan.FromMilliseconds(100))
                .SendKeys(password)
                .Perform();
            return this;
        }

        public LoginPage SetRememberMe()
        {
            rememberMeCheckbox.Click();
            return this;
        }

        public ProjectsPage ClickLogin()
        {
            signInButton.Click();
            return new ProjectsPage();
        }
    }
}
