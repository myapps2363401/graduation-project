﻿global using NUnit.Framework;
using Core.Reporting;
using Core.Selenium;
using NUnit.Allure.Core;

namespace Qase.Ui.Tests.AllTests
{
    [AllureNUnit]
    [Parallelizable(ParallelScope.Fixtures)]
    internal class BaseUiTest : Base
    {
        protected AllureReport report;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            report = new(driver);
            report.allure.CleanupResultDirectory();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            browser.Close();
        }
    }
}
