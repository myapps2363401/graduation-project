﻿namespace Qase.Api.Tests.Entities
{
    public class CaseRequest
    {
        public string description { get; set; }
        public string preconditions { get; set; }
        public string postconditions { get; set; }
        public string title { get; set; }
        public int severity { get; set; }
        public int priority { get; set; }
        public int behavior { get; set; }
        public int type { get; set; }
        public int layer { get; set; }
        public int is_flaky { get; set; }
        public int suite_id { get; set; }
        public int milestone_id { get; set; }
        public int automation { get; set; }
        public int status { get; set; }
        public int created_at { get; set; }
        public int updated_at { get; set; }
    }

    public class CaseResponse
    {
        public bool status { get; set; }
        public Result result { get; set; }
    }
}
