﻿using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

namespace Core
{
    public static class Logger
    {
        public static readonly ILogger Instance = _log ??= CreateLogger();
        private static ILogger _log;

        private static ILogger CreateLogger()
        {
            return new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Console(theme: AnsiConsoleTheme.Code)
                .WriteTo.File("log.txt", rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true)
                .CreateLogger();
        }
    }
}