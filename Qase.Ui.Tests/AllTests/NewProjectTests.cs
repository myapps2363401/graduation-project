﻿using Qase.Web.Steps;

namespace Qase.Ui.Tests.AllTests
{
    [TestFixture]
    internal class NewProjectTests : BaseUiTest
    {
        private ProjectsSteps newProjectSteps;

        [SetUp]
        public void SetUp()
        {
            browser.Open(baseUrl);
            newProjectSteps = new();
        }

        [Category("Smoke")]
        [Test]
        public void NewProjectTest()
        {
            newProjectSteps.AddNewPublicProject();
            Assert.IsTrue(newProjectSteps.projectsPage.createProjectButton.Displayed);
            newProjectSteps.CreateProject();
        }

        [TearDown]
        public void TearDown()
        {
            report.GetErrorScreenshot();
        }
    }
}
