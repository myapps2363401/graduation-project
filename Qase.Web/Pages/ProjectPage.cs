﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Qase.Web.Pages
{
    public class ProjectPage : BasePage
    {

        [FindsBy(How = How.Id, Using = "create-case-button")]
        public readonly IWebElement caseButton;

        public ProjectPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public ProjectPage CreateCase()
        {
            caseButton.Click();
            return this;
        }
    }
}
