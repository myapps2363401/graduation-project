﻿using Qase.Web.Steps;

namespace Qase.Ui.Tests.AllTests
{
    [TestFixture]
    internal class LoginPageTests : BaseUiTest
    {
        private LoginSteps loginSteps = new();

        [SetUp]
        public void SetUp()
        {
            browser.Open(baseUrl);
        }

        [Category("Smoke")]
        [Test]
        public void LoginTest()
        {
            loginSteps.Login();
            Assert.IsTrue(loginSteps.loginPage.rememberMeCheckbox.Enabled);
            loginSteps.GoToWorkspace();
            Assert.AreEqual(browser.Driver.Url, "https://app.qase.io/projects");
        }

        [TearDown]
        public void TearDown()
        {
            report.GetErrorScreenshot();
        }
    }
}
