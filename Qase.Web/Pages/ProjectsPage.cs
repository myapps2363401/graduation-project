﻿using OpenQA.Selenium;
using Qase.Web.UI.RadioButtons;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;

namespace Qase.Web.Pages
{
    public class ProjectsPage : BasePage
    {
        #region CreateNewProject

        [FindsBy(How = How.Id, Using = "createButton")]
        public readonly IWebElement createNewProjectButton;

        [FindsBy(How = How.Id, Using = "project-name")]
        public readonly IWebElement projectNameInput;

        [FindsBy(How = How.Id, Using = "project-code")]
        public readonly IWebElement projectCodeInput;

        [FindsBy(How = How.Name, Using = "description-area")]
        public readonly IWebElement descriptionInput;

        [FindsBy(How = How.CssSelector, Using = ".mVyEPf.L8Bw8i")]
        public readonly IWebElement projectAccessTypeRadioButton;

        [FindsBy(How = How.CssSelector, Using = ".g_pa6L.L8Bw8i")]
        public readonly IWebElement memberAccessRadioButton;

        [FindsBy(How = How.CssSelector, Using = ".j4xaa7.Jn_dMk.J4xngT")]
        public readonly IWebElement cancelButton;

        [FindsBy(How = How.CssSelector, Using = ".svg-inline--fa.fa-xmark ")]
        public readonly IWebElement cancelIcon;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit']")]
        public readonly IWebElement createProjectButton;

        #endregion

        [FindsBy(How = How.XPath, Using = "//button[@class='j4xaa7 bB3U2Y TuZZEp']")]
        public readonly IWebElement meatballButton;

        [FindsBy(How = How.XPath, Using = "//button[@class='KXyzbV IhDC1_ rHqCyR']")]
        public readonly IWebElement removeButton;

        [FindsBy(How = How.XPath, Using = "//button[@class='j4xaa7 b_jd28 J4xngT']")]
        public readonly IWebElement deleteProjectButton;

        public ProjectsPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public ProjectsPage ClickButton(IWebElement element)
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
            element.Click();
            return this;
        }

        public ProjectsPage SetField(IWebElement element, string text)
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
            element.SendKeys(text);
            return this;
        }

        public ProjectsPage SetProjectAccessType(ProjectAccessTypeEnum type)
        {
            projectAccessTypeRadioButton.FindElement(By.XPath($".//span[text()='{type}']")).Click();
            return this;
        }

        public ProjectsPage SetMemberAccess(string memberAccess)
        {
            memberAccessRadioButton.FindElement(By.XPath($".//span[text()='{memberAccess}']")).Click();
            return this;
        }

        public ProjectPage CreateProject()
        {
            createProjectButton.Click();
            return new ProjectPage();
        }

        public ProjectsPage DeleteProject()
        {
            action.Pause(TimeSpan.FromMicroseconds(100))
                .Click(meatballButton)
                .Pause(TimeSpan.FromMicroseconds(100))
                .Click(removeButton)
                .Pause(TimeSpan.FromMicroseconds(100))
                .Click(deleteProjectButton)
                .Perform();

            return this;
        }
    }
}
